import urllib2
#from bs4 import BeautifulSoup
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

base_url = "http://www.ultianalytics.com/rest/view/team/xxx/stats/export"
team_ids= {'atlanta':'5664284977659904','austin':'5128666635829248','charlotte':'5640136574369792','chicago':'5691616589250560','cincinnati':'5069252692279296','DC':'5705148370255872','Dallas':'5206956054675456'}


data={}
check_sum = []

for key in team_ids.keys():
    new = base_url.replace("xxx",str(team_ids[key]))
    resp=urllib2.urlopen(new)
    test = pd.read_csv(resp)
    check_sum.append(len(test))
    data[key]=test

#print data
df = pd.concat(data)
print df.head()

cols = df.columns.tolist()

f = {'Count':['count'],'Avg':['mean']}
pulls = df[df['Hang Time (secs)'].notnull()]
t = pulls.groupby('Defender')['Hang Time (secs)'].describe()
print t.unstack()

#plt.show()
#
#for col in cols:
#    print col,np.unique(df[col].values)
#    
    