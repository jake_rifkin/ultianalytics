import urllib2
import pandas as pd
import time
from tqdm import *
import pickle as pkl
import os
from boto.s3.connection import S3Connection
from boto.s3.key import Key


def single_team_test(team_id):
    """A function to test a single team."""
    print '******TESTING UNIT*******'

    # needed to specify which columns to keep as C pandas read_csv interpreter
    # was getting confused by data appearing not at the beginning but half way
    # through a column

    # place to save files
    # target_path = "/Users/jrifkin/Downloads/MadisonRadicals2015-stats.csv"
    base_url = "http://www.ultianalytics.com/rest/view/team/xxx/stats/export"

    url = base_url.replace('xxx', str(team_id))
    dat = urllib2.urlopen(url)
    data = pd.read_csv(dat)
    # data = pd.read_csv(target_path)
    print data.head()
    print data.iloc[722]
    # print sorted(data.columns)


def get_data(teams, year, local=True, verbose=False):
    """A method to download all csvs from ultianalytics.com for AUDL."""
    # url to get data
    base_url = "http://www.ultianalytics.com/rest/view/team/xxx/stats/export"
    keep_cols = ['Absolute Distance', 'Action', 'Begin Area', 'Begin X',
                 'Begin Y', 'Date/Time', 'Defender',
                 'Distance Unit of Measure', 'Elapsed Time (secs)', 'End Area',
                 'End X', 'End Y', 'Event Type', 'Hang Time (secs)',
                 'Lateral Distance', 'Line', 'Opponent',
                 'Our Score - End of Point', 'Passer', 'Player 0',
                 'Player 1', 'Player 10', 'Player 11', 'Player 12',
                 'Player 13', 'Player 14', 'Player 15', 'Player 16',
                 'Player 17', 'Player 18', 'Player 19', 'Player 2',
                 'Player 20', 'Player 21', 'Player 22', 'Player 23',
                 'Player 24', 'Player 25', 'Player 26', 'Player 27',
                 'Player 3', 'Player 4', 'Player 5', 'Player 6', 'Player 7',
                 'Player 8', 'Player 9', 'Point Elapsed Seconds', 'Receiver',
                 'Their Score - End of Point', 'Tournamemnt',
                 'Toward Our Goal Distance']

    # choose where to save
    if local:
        # local path
        target_path = "/Users/jrifkin/Documents/Internal Projects/ultianalytics/\
        data"
    else:
        # create S3 connection
        conn = S3Connection(os.environ['AWS_ACCESS_KEY_ID'],
                            os.environ['AWS_SECRET_ACCESS_KEY'])
        bucket = conn.get_bucket(os.environ['AWS_BUCKET_NAME'])
        print "Connected", conn

    frames = []
    for k, v in teams.iteritems():
        if verbose:
            print "Getting %s data" % k

        new = base_url.replace("xxx", str(v))
        dat = urllib2.urlopen(new)
        frames.append(pd.read_csv(dat, usecols=keep_cols))

    team_data = pd.concat(frames, keys=teams.keys())
    if local:
        # place to save files
        team_data.to_csv(target_path + "/" + str(year) + "-audl-data.csv")
    else:
        # save to S3
        s3_key = Key(bucket)
        s3_key.key = str(year) + "-audl-data.pkl"
        team_data.to_pickle(str(year) + "-audl-data.pkl")
        s3_key.set_contents_from_file(open(str(year) + "-audl-data.pkl", 'r'))


def get_ids():
    """Function to return ids."""
    with open("team_config.pkl") as f:
        teams = pkl.load(f)
    return teams


def main():
    """Main function."""
    ids = get_ids()
    audl_base_year = 2014

    # OPTIONAL TEST FUNCTION
    # single_team_test(team_ids_2015['madison'])

    # iterate through years and save to either local files or S3
    for i, ulti_id in tqdm(enumerate(ids)):
        y = i + audl_base_year
        print "Downloading %s AUDL Data" % (y)
        get_data(ulti_id, y, local=False, verbose=False)


if __name__ == "__main__":
    try:
        start_time = time.time()
        main()
        print "--- %s seconds ---" % (time.time() - start_time)
    except Exception, e:
        print 'Error'
        print e.message
